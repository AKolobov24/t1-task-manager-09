package ru.t1.akolobov.tm.api;

import ru.t1.akolobov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
