package ru.t1.akolobov.tm.component;

import ru.t1.akolobov.tm.api.ICommandController;
import ru.t1.akolobov.tm.api.ICommandRepository;
import ru.t1.akolobov.tm.api.ICommandService;
import ru.t1.akolobov.tm.controller.CommandController;
import ru.t1.akolobov.tm.repository.CommandRepository;
import ru.t1.akolobov.tm.service.CommandService;

import java.util.Scanner;

import static ru.t1.akolobov.tm.constant.ArgumentConst.*;
import static ru.t1.akolobov.tm.constant.ArgumentConst.ARG_COMMANDS;
import static ru.t1.akolobov.tm.constant.TerminalConst.*;
import static ru.t1.akolobov.tm.constant.TerminalConst.CMD_EXIT;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private boolean isCommandMode = false;

    public void run(final String[] args) {
        processArguments(args);
        isCommandMode = true;
        commandController.displayWelcome();
        processCommands();
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            processCommand(command);
            System.out.println();
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) {
            commandController.displayError(isCommandMode);
            return;
        }
        switch (command) {
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_INFO:
                commandController.displayInfo();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                commandController.displayError(isCommandMode);
        }
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case ARG_VERSION:
                commandController.displayVersion();
                break;
            case ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ARG_HELP:
                commandController.displayHelp();
                break;
            case ARG_INFO:
                commandController.displayInfo();
                break;
            case ARG_ARGUMENTS:
                commandController.displayArguments();
                break;
            case ARG_COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayError(isCommandMode);
        }
    }

    private void exit() {
        System.exit(0);
    }

}
